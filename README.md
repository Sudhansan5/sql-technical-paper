# SQL

## Overview

SQL stands for Structured Query Language. It is used for database creation, deletion, fetching rows, and modifying rows, etc. It is used when we have structured data (in the form of tables).

## Applications of SQL

SQL has many applications. Few of them are :

1. To create new databases, tables and views.
2. To insert records in a database.
3. To update records in a database.
4. To delete records from a database.
5. To retrieve data from a database.

Now let us see a few common queries and their syntax. Let's consider a table "Student" for better understanding.

| ID | Candidate |    Subject     |  Marks |
|----|-----------|:--------------:|-------:|
| 1  | Sonu      |    Physics     |   43   |
| 2  | Rakshit   |    English     |   98   |
| 3  | Saket     |    Computer    |   82   |

## SELECT Queries

The most commonly used SQL command is the SELECT statement. It is used to query the database and retrieve selected data that follow the conditions we want i.e., to retrieve data from a SQL database we need to write SELECT statements.

It's basic syntax is :

```SQL
SELECT column1,column2
FROM table_name;
```

* **Example**

* Show all the candidates from the Student table.

```SQL
SELECT candidate FROM Student;
```

* **OUTPUT**

|Candidate |
|----------|
|  Sonu    |
|  Rakshit |
|  Saket   |

## Queries with constraints

We can give constraints to our query to filter out any particular set of rows and columns. So, if we want to fetch data based on a certain condition, we use Queries with constraints.

Syntax:

```SQL
SELECT column, another_column, …
FROM mytable
WHERE condition
    AND/OR another_condition
    AND/OR …;
```

* **Example**

* Get the name and marks of the candidate whose marks is more than 50.

```SQL
SELECT candidate, marks FROM Student
WHERE marks>50;
```

* **OUTPUT**

| Candidate |  Marks |
|-----------|:------:|
| Rakshit   |   98   |
| Saket     |   82   |

## JOINS

If there are two or more tables, we use JOIN to combine rows and columns based on a related column between them.

### Types of SQL JOIN

There are four types of JOIN:

1. (INNER) JOIN: Returns records that have matching values in both tables.
2. LEFT (OUTER) JOIN: Returns all records from the left table, and the matched records from the right table.
3. RIGHT (OUTER) JOIN: Returns all records from the right table, and the matched records from the left table.
4. FULL (OUTER) JOIN: Returns all records when there is a match in either the left or the right table.

![alt text](images/joins.png "Title")

Basic syntax of JOIN:

```SQL
SELECT column, another_column, …
FROM mytable
INNER/LEFT/RIGHT/FULL JOIN another_table
    ON mytable.id = another_table.matching_id
WHERE condition(s);
```

Now let's consider two tables as shown below to understand JOINS.

* **Student**

| ID | Candidate |    Subject     |  Marks |
|----|-----------|:--------------:|-------:|
| 1  | Sonu      |    Physics     |   43   |
| 2  | Rakshit   |    English     |   98   |
| 3  | Saket     |    Computer    |   82   |

* **Details**

| ID | Class     |    Teacher     |
|----|:---------:|:--------------:|
| 1  | 8         |    Shekhar     |
| 2  | 9         |    Navin Reddy |
| 3  | 10        |    Naman Mathur|

* **Example**

* Get the candidate name, subject, class, teacher from tables 'Student' and 'Details'.

```SQL
SELECT candidate, subject, class, teacher FROM Student INNER JOIN Details
ON Student.ID = Details.ID;
```

* **OUTPUT**

| Candidate |    Subject     |  Class | Teacher    |
|-----------|:--------------:|-------:|------------|
| Sonu      |    Physics     |   8    |Shekhar     |
| Rakshit   |    English     |   9    |Navin Reddy |
| Saket     |    Computer    |   10   |Naman Mathur|

## Queries with aggregates

We can perform various mathematical operations and fetch data from the table according to it. When aggregate functions are used, the values of multiple rows are grouped together as input on certain criteria to form a single value of more significant meaning.

Few common aggregate functions are:

1. Count()
2. Sum()
3. Avg()
4. Min()
5. Max()

Let's see a few examples of aggregate functions on table 'Student':

* **Student**

| ID | Candidate |    Subject     |  Marks |
|----|-----------|:--------------:|-------:|
| 1  | Sonu      |    Physics     |   43   |
| 2  | Rakshit   |    English     |   98   |
| 3  | Saket     |    Computer    |   82   |

* Count total number of candidates from the Student table.

```SQL
SELECT count(candidate) as total_candidates from Student;
```

OUTPUT

|  total_candidates    |
|:--------------------:|
|  3                   |

* Show minimum, maximum marks and average of all the marks from Student table.

```SQL
SELECT min(marks), max(marks), avg(marks) from Student;
```

OUTPUT

| min(marks) |    max(marks)     |  avg(marks) |
|:----------:|:-----------------:|:-----------:|
| 43         |    98             |   74.33     |

## References

1. [SQL Bolt](https://sqlbolt.com)
2. [GeeksforGeeks](https://www.geeksforgeeks.org/sql-tutorial)
3. [W3Schools](https://www.w3schools.com/sql/default.asp)
